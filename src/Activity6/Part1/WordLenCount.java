package Activity6.Part1;

import java.io.IOException;
import java.util.StringTokenizer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.Path;

public class WordLenCount
{
	public static class EdxMap extends Mapper<LongWritable,Text,Text,IntWritable> {
		public void map(LongWritable key, Text value,Context context) throws IOException,InterruptedException{
			String line = value.toString().toLowerCase();
			StringTokenizer tokenizer = new StringTokenizer(line);
			while (tokenizer.hasMoreTokens()) {
				value.set( tokenizer.nextToken() );
				context.write(value, new IntWritable(1));
			}
		}
	}

	public static class EdxReduce extends Reducer<Text,IntWritable,Text,IntWritable> {
		public void reduce(Text key, Iterable<IntWritable> values,Context context) throws IOException,InterruptedException {
			int sum=0;
			for(IntWritable x: values)
			{
				sum+=x.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	public static class EdxCombiner extends Reducer<Text,IntWritable,Text,IntWritable> {
		public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException
	    {
			int sum=0;
			for (IntWritable x: values)
			{
				sum+=x.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	public static class SecondMap extends Mapper<LongWritable,Text,IntWritable,IntWritable> {
		public void map(LongWritable key, Text value,Context context) throws IOException,InterruptedException{
			String line = value.toString().toLowerCase();
			StringTokenizer tokenizer = new StringTokenizer(line);
			IntWritable out_value = new IntWritable();
			int i = 0;
			while (i++ < 1) {
				out_value.set( tokenizer.nextToken().length()  );
				context.write(out_value, new IntWritable(1));
			}
			
		}
	}

	public static class SecondReduce extends Reducer<IntWritable,IntWritable,IntWritable,IntWritable> {
		public void reduce(IntWritable key, Iterable<IntWritable> values,Context context) throws IOException,InterruptedException {
			int sum=0;
			for(IntWritable x: values)
			{
				sum+=x.get();
			}
			context.write(key, new IntWritable(sum));
		}
	}

	public static void main(String[] args) throws Exception {
		
		Job job = Job.getInstance();
		job.setJarByClass(WordLenCount.class);
		job.setJobName("wordLenCounter");
		
		job.setMapperClass(EdxMap.class);
		job.setCombinerClass(EdxCombiner.class);	
		job.setReducerClass(EdxReduce.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		//
		job.waitForCompletion(true);
//
		Job job2 = Job.getInstance();
		job2.setJarByClass(WordLenCount.class);
		job2.setJobName("wordLenCounter-2");
		
		job2.setMapperClass(SecondMap.class);
		job2.setReducerClass(SecondReduce.class);
		
		job2.setOutputKeyClass(IntWritable.class);
		job2.setOutputValueClass(IntWritable.class);
		
		job2.setInputFormatClass(TextInputFormat.class);
		job2.setOutputFormatClass(TextOutputFormat.class);

		FileInputFormat.addInputPath(job2, new Path(args[1]+"/part-r-00000"));
		FileOutputFormat.setOutputPath(job2, new Path("output-2"));

//		
		
		job2.waitForCompletion(true);
		
		System.exit(0);
	}

}